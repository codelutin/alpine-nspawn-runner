FROM alpine:3.21

RUN apk add --no-cache alpine-base docker dropbear fuse-overlayfs gitlab-runner

COPY fs/ /

RUN sed -i '/tty[0-9]:/ s/^/#/' /etc/inittab && \
    echo DROPBEAR_OPTS="-s" >> /etc/conf.d/dropbear && \
    mkdir -m 500 /root/.ssh && \
    adduser gitlab-runner docker && \
    chmod 755 /etc/init.d/chown-runner-config && \
    # Add services
    for s in dropbear syslog; do ln -s /etc/init.d/$s /etc/runlevels/boot/$s; done && \
    for s in chown-runner-config crond docker gitlab-runner; do ln -s /etc/init.d/$s /etc/runlevels/default/$s; done && \
    for s in killprocs savecache; do ln -s /etc/init.d/$s /etc/runlevels/shutdown/$s; done && \
    # Remove unsafe perms
    find etc opt root usr -perm /og+w -a \( -type f -o -type d \) -exec chmod og-w {} +
