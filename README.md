# alpine-nspawn-runner

An alpine-powered container with Docker, gitlab-runner and openrc, designed to be run with systemd-nspawn.

## Overview

As systemd is not available in Alpine, we use openrc, so `machinectl shell` commands wont work. We access the container via SSH.

## Usage (infrastructure as code workflow)

### Prerequisites

- Root access to a server with systemd-nspawn (for the moment, only Debian 11 has been tested)
- A private repo on some GitLab instance

### First install

1. Place your public SSH key in a `authorized_keys` file in the repo
2. Create a Dockerfile, add the `authorized_keys` file and optionally other config, for example:
   ```Dockerfile
   FROM registry.nuiton.org/codelutin/alpine-nspawn-runner:latest

   COPY authorized_keys /root/.ssh/authorized_keys
   COPY daemon.json /etc/docker/daemon.json
   ```
3. Setup CI/CD to build your image, and export the resulting filesystem as a `.tar.gz`
4. On the server, as root, download and install the runner:
   ```sh
   # Adapt these variables
   RUNNER_HOSTNAME=my-runner
   PACKAGE_URL=https://your_package_url

   mkdir -p "/var/alpine-nspawn-runner/machines/$RUNNER_HOSTNAME/v0.1.0"
   wget -qO- "$PACKAGE_URL" | tar -xz -C "/var/alpine-nspawn-runner/machines/$RUNNER_HOSTNAME/v0.1.0"
   # The install script will setup the nspawn container and guide you with runner registration
   /var/alpine-nspawn-runner/machines/your_machine/v0.1.0/install.sh "$RUNNER_HOSTNAME"
   ```
5. (optional) In order to have a readable output for `docker build` commands in your CI job logs, add a `BUILDKIT_PROGRESS=plain` variable (globally if you are admin of the GitLab instance, in the project CI/CD variables otherwise)

### Updates

1. Download a new version of the filesystem:
   ```console
   # mkdir -p /var/alpine-nspawn-runner/machines/your_machine/v0.2.0
   # wget -qO- "https://your_package_url" | tar -xz -C "/var/alpine-nspawn-runner/machines/your_machine/v0.2.0"
   ```
2. Run the installation script from the host: `/var/alpine-nspawn-runner/machines/your_machine/v0.2.0/install.sh`
   It will stop the current nspawn container, rsync the new fs into the old one and launch a new nspawn container. By default, the directories listed in [rsync.excludes](./rsync.excludes) are excluded from the sync.

