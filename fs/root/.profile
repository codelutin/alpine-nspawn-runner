# Custom prompt
RED='\[\e[1;31m\]'
YELLOW='\[\e[1;33m\]'
BLUE='\[\e[1;34m\]'
NC='\[\e[0m\]' # No Color
PS1="${YELLOW}[\t] ${RED}\u@\h${NC}:${BLUE}\w${NC}\\$ "
