#!/bin/sh

set -e

[ $# -ne 1 ] && echo "Usage: $0 <RUNNER_HOSTNAME>" >&2 && exit 1

DIR="$(dirname "$(realpath "$0")")"
RUNNER_HOSTNAME="$1"
RUNNER_HOME="/var/lib/machines/$RUNNER_HOSTNAME"
RUNNER_CONFIG="$RUNNER_HOME/etc/gitlab-runner/config.toml"
RSYNC_EXCLUDES="$DIR/opt/alpine-nspawn-runner/rsync.excludes"
RUNNER_TOKEN_FILE="/etc/secrets/gitlab_runner_token/$RUNNER_HOSTNAME"

systemctl is-active "systemd-nspawn@$RUNNER_HOSTNAME" && systemctl disable --now "systemd-nspawn@$RUNNER_HOSTNAME"

mkdir -pv "$RUNNER_HOME"

rsync \
	--archive \
	--hard-links \
	--human-readable \
	--verbose \
	--exclude-from="$RSYNC_EXCLUDES" \
	--delete \
	--info=progress2 \
	"$DIR/" "$RUNNER_HOME/"

echo "hostname=$RUNNER_HOSTNAME" > "$RUNNER_HOME/etc/conf.d/hostname"

# Populate /etc/hosts
# We can't add a hosts file to the Docker image because Docker will override it
cat << EOF > "$RUNNER_HOME/etc/hosts"
127.0.0.1	localhost
::1	localhost ip6-localhost ip6-loopback
fe00::0	ip6-localnet
ff00::0	ip6-mcastprefix
ff02::1	ip6-allnodes
ff02::2	ip6-allrouters
EOF

mkdir -pv -m 700 /etc/systemd/nspawn
cp -v "$RUNNER_HOME/opt/alpine-nspawn-runner/runner.nspawn" "/etc/systemd/nspawn/$RUNNER_HOSTNAME.nspawn"

systemctl set-property "systemd-nspawn@$RUNNER_HOSTNAME" DeviceAllow='/dev/fuse rwm'

if [ -f "$RUNNER_CONFIG" ]; then
	# Ensure permissions are safe
	chmod -v 600 "$RUNNER_CONFIG"

	if [ -f "$RUNNER_TOKEN_FILE" ]; then
		echo "Found RUNNER_TOKEN_FILE ($RUNNER_TOKEN_FILE)"
		echo "Updating RUNNER_CONFIG ($RUNNER_CONFIG)"
		sed -i -f "$RUNNER_TOKEN_FILE" "$RUNNER_CONFIG"
	fi

	systemctl enable --now "systemd-nspawn@$RUNNER_HOSTNAME"
else
	cat <<- EOF
	gitlab-runner config not found. If you have a config, place it on $RUNNER_CONFIG. Or you can register the runner with:

	  systemd-nspawn --machine=$RUNNER_HOSTNAME gitlab-runner register --executor docker --docker-image alpine --tag-list docker

	Then, remove the  enable and launch the runner with:

	  systemctl enable --now "systemd-nspawn@$RUNNER_HOSTNAME"

	EOF
fi
